import { newArray } from '@angular/compiler/src/util';
import { Component, OnInit, ViewChild } from '@angular/core';

import { Record } from './models/record';
import { DataService } from './services/data.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  app_title = 'Project Dashboard';
  allRecords!: Record[];
  allTitles: string[] = [];
  allDivisions: string[] = [];
  allProjectOwners: string[] = [];
  allBudgets: number[] = [];
  allStatuses: string[] = [];
  filteredRecords!: Record[];
  selectedOptions: Record[] = [];
  showFewerRecsLink: boolean = false;
  showAllRecords: boolean = true;
  showFilteredRecords: boolean = false;
  show_filter_section: boolean = true;
  isEdit: boolean = false;
  selectedTitle: any;
  selectedDivision: any;
  selectedProjectOwner: any;
  selectedBudget: any;
  selectedStatus: any;
  filtered: boolean = false;

  @ViewChild('showMoreRecsLink') public showMoreRecsLink: any;
  @ViewChild('filterDropdown') public filterDropdown: any;
  @ViewChild('recordsWrapper') public recordsWrapper: any;
  @ViewChild('title_filter') public title_filter: any;
  @ViewChild('division_filter') public division_filter: any;
  @ViewChild('project_owner_filter') public project_owner_filter: any;
  @ViewChild('budget_filter') public budget_filter: any;
  @ViewChild('status_filter') public status_filter: any;
  @ViewChild('created_date_filter') public created_date_filter: any;
  @ViewChild('modified_date_filter') public modified_date_filter: any;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.filteredRecords = [];
    this.dataService.getAllRecords()
      .subscribe(
        (data: Record[]) => {
          this.allRecords = data;
          this.allTitles = this.getRecordProps(this.allRecords, 'title');
          this.allDivisions = this.getRecordProps(this.allRecords, 'division');
          this.allProjectOwners = this.getRecordProps(this.allRecords, 'project_owner');
          this.allBudgets = this.getRecordProps(this.allRecords, 'budget');
          this.allStatuses = this.getRecordProps(this.allRecords, 'status');
        },
        err => console.log(err),
        () => console.log('All done getting records.')
      );
  }

  getRecordProps(allRecords:Record[], item:any) {
    let record_prop:any[] = []; 
    allRecords.forEach(record => {
      switch(item) {
        case 'title' : record_prop.push(record.title); break;
        case 'division' : record_prop.push(record.division); break;
        case 'project_owner' : record_prop.push(record.project_owner); break;
        case 'budget' : record_prop.push(record.budget.toLocaleString('en-US', {
          style: 'currency',
          currency: 'USD',
        })); break;
        case 'status' : record_prop.push(record.status); break;
      }     
    });    
    return record_prop.filter((item, i) => i === record_prop.indexOf(item));
  }

  toggleRecords(event: any) {
    let records: any = Array.from(document.getElementsByClassName('rec') as HTMLCollectionOf<HTMLElement>);
    if (event.target.innerText == "Load More Offers") {
      event.target.textContent = "Show Fewer Records";
      this.showFewerRecsLink = true;
      for (let i = 0; i < records.length; i++) {
        records[i].classList.remove('hidden')
      }
    }
    else if (event.target.innerText == "Show Fewer Records") {
      this.showMoreRecsLink.nativeElement.innerText = "Load More Offers";
      this.showFewerRecsLink = false;
      for (let i = 0; i < records.length; i++) {
        if (i > 7) {
          records[i].classList.add('hidden')
        }
      }
    }
  }

  toggleFilterSection(event: any) {
    if (event.target.innerText == "Show Filters") {
      event.target.textContent = "Hide Filters";
      this.show_filter_section = true;      
    } else if (event.target.innerText == "Hide Filters") {
      event.target.textContent = "Show Filters";
      this.show_filter_section = false;
    }
  }

  updateItem(record: Record, event: any, index: any, update_type: string, rec_prop: string) {
    let edit_po_el = document.querySelector('.rec #edit-po-'+index) as HTMLElement;
    let read_po_el = document.querySelector('.rec #read-po-'+index) as HTMLElement;
    let edit_bgt_el = document.querySelector('.rec #edit-bgt-'+index) as HTMLElement;
    let read_bgt_el = document.querySelector('.rec #read-bgt-'+index) as HTMLElement;
    let edit_sts_el = document.querySelector('.rec #edit-sts-'+index) as HTMLElement;
    let read_sts_el = document.querySelector('.rec #read-sts-'+index) as HTMLElement;

    if (rec_prop == 'po') {
      console.log(edit_po_el);
      console.log(read_po_el);
      let input_el = document.querySelector('.rec #edit-po-'+index+' input') as HTMLInputElement;
      if (update_type == 'edit') {
        edit_po_el.classList.remove('hidden')
        read_po_el.classList.add('hidden')
      } else if (update_type == 'save') {
        edit_po_el.classList.add('hidden')
        read_po_el.classList.remove('hidden')
        record.project_owner = input_el.value;

        let req_data = {
          title: record.title,
          division: record.division,
          project_owner: input_el.value,
          budget: record.budget,
          status: record.status,
          created: record.created,
          modified: record.modified
        }          
        console.log(req_data)
        alert('Record Project Owner updated to '+record.project_owner)        
      }
    } else if (rec_prop == 'bgt') {
      let input_el = document.querySelector('.rec #edit-bgt-'+index+' input') as HTMLInputElement;
      if (update_type == 'edit') {
        edit_bgt_el.classList.remove('hidden')
        read_bgt_el.classList.add('hidden')
      } else if (update_type == 'save') {
        edit_bgt_el.classList.add('hidden')
        read_bgt_el.classList.remove('hidden')
        record.budget = Number(input_el.value);
        alert('Budget updated to '+record.budget) 
      }
    } else if (rec_prop == 'sts') {
      console.log(index)
      let sts_sel_el = document.querySelector('.rec #edit-sts-'+index+' select') as HTMLSelectElement;
      if (update_type == 'edit') {
        edit_sts_el.classList.remove('hidden')
        read_sts_el.classList.add('hidden')
      } else if (update_type == 'save') {
        edit_sts_el.classList.add('hidden')
        read_sts_el.classList.remove('hidden')       
        let selected_val = sts_sel_el.options[sts_sel_el.selectedIndex].text;
        record.status = selected_val;
        alert('Record Status updated to '+record.status) 
      }
    }
  }

  dateRangeCreated($event: any) {
    let startDate = $event[0].toJSON().split('T')[0];  
    let endDate = $event[1].toJSON().split('T')[0];  
    this.allRecords = this.allRecords.filter(
      m => new Date(m.created) >= new Date(startDate) && new Date(m.created) <= new Date(endDate)
    );
  }

  dateRangeModified($event: any) {
    let startDate = $event[0].toJSON().split('T')[0];  
    let endDate = $event[1].toJSON().split('T')[0];  
    this.allRecords = this.allRecords.filter(
      m => new Date(m.modified) >= new Date(startDate) && new Date(m.modified) <= new Date(endDate)
    );
  }


  filterItem(event: any, record_prop: any) {
    
    if (record_prop == 'title') {
      console.log(this.title_filter.nativeElement.options[event.target.selectedIndex].text);
      this.filteredRecords = this.allRecords.filter(record => {
        if ((this.title_filter.nativeElement.options[event.target.selectedIndex].text).trim().length == 0) {
          console.log('here')
          return record.title
        } else if (this.title_filter.nativeElement.options[event.target.selectedIndex].text.trim().length > 0) {
          return record.title == this.title_filter.nativeElement.options[event.target.selectedIndex].text
        }
      }
      );
    }

    if (record_prop == 'division') {
      this.filteredRecords = this.allRecords.filter(record => {
        if ((this.division_filter.nativeElement.options[event.target.selectedIndex].text).trim().length == 0) {
          console.log('here')
          return record.division
        } else if (this.division_filter.nativeElement.options[event.target.selectedIndex].text.trim().length > 0) {
          return record.division == this.division_filter.nativeElement.options[event.target.selectedIndex].text
        }
      }
      );
    }

    if (record_prop == 'project_owner') {
      this.filteredRecords = this.allRecords.filter(record => {
        if ((this.project_owner_filter.nativeElement.options[event.target.selectedIndex].text).trim().length == 0) {
          console.log('here')
          return record.project_owner
        } else if (this.project_owner_filter.nativeElement.options[event.target.selectedIndex].text.trim().length > 0) {
          return record.project_owner == this.project_owner_filter.nativeElement.options[event.target.selectedIndex].text
        }
      }
      );
    }

    if (record_prop == 'budget') {
      this.filteredRecords = this.allRecords.filter(record => {
        console.log(record.budget)
        let budget = record.budget.toLocaleString('en-US', {
          style: 'currency',
          currency: 'USD',
        });
        if ((this.budget_filter.nativeElement.options[event.target.selectedIndex].text).trim().length == 0) {
          console.log('here')
          return budget
        } else if (this.budget_filter.nativeElement.options[event.target.selectedIndex].text.trim().length > 0) {
          return budget == this.budget_filter.nativeElement.options[event.target.selectedIndex].text
        }
      }
      );
    }

    if (record_prop == 'status') {
      this.filteredRecords = this.allRecords.filter(record => {
        if ((this.status_filter.nativeElement.options[event.target.selectedIndex].text).trim().length == 0) {
          console.log('here')
          return record.status
        } else if (this.status_filter.nativeElement.options[event.target.selectedIndex].text.trim().length > 0) {
          return record.status == this.status_filter.nativeElement.options[event.target.selectedIndex].text
        }
      }
      );
    }

    if (record_prop == 'date_created') {
      console.log(event)
      let startDate = event[0].toJSON().split('T')[0];  
      let endDate = event[1].toJSON().split('T')[0];  
      this.allRecords = this.allRecords.filter(
        m => new Date(m.created) >= new Date(startDate) && new Date(m.created) <= new Date(endDate)
      );
    }

    if (record_prop == 'date_modified') {
      let startDate = event[0].toJSON().split('T')[0];  
      let endDate = event[1].toJSON().split('T')[0];  
      this.allRecords = this.allRecords.filter(
        m => new Date(m.modified) >= new Date(startDate) && new Date(m.modified) <= new Date(endDate)
      );
    }

    this.filtered = true;
  }

}
