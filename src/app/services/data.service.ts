
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Record } from '../models/record';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    api_url: string = 'http://localhost:3000/records';

    constructor(private http: HttpClient) { }

    getAllRecords(): Observable<Record[]> {
        console.log('Get all Records')
        return this.http.get<Record[]>(this.api_url);
    }

    updateRecord(record: Record) {
        console.log('Update record', record)
        return this.http
                .put(this.api_url, JSON.stringify(record))
                .pipe(map(this.extractData),
                catchError(this.handleError))
    }

    extractData(res: any) {
        if (res && typeof res == 'object') {
            return res;
        } else {
            return res.json() || {}
        }
    }

    handleError(error: any) {
        return throwError({ errMsg: error})
    }

}
